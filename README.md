Cinnamon Applet: Configurable Menu Version: v0.9-Beta
--------------
Last version release date: 21 January 2014

Authors: [Lester Carballo Pérez](https://gitlab.com/lestcape) and [Garibaldo](https://github.com/Garibaldo).

Contact: lestcape@gmail.com


Special thanks to:
--------------

- [Bernard](https://github.com/zagortenay333) (Help adding support to the Cinnamon themes).
- Eliermi Cunha Junior(jpegrande@gmail.com) (Help finding errors and making several promotional videos).

--------------
![](https://gitlab.com/lestcape/Configurable-Menu/wikis/img/Capture.png)

Description
--------------
Configurable Menu is a fork of the Cinnamon stock menu applet. It has much more features and is extremely configurable. This latest version brings a lot of cool new stuff, like:

- Choose among many different menu layouts.
- Choose different system buttons layouts.
- Define the exact size of the menu (width and height) also with the mouse.
- Show the menu in full screen mode.
- Show/Hide and change the size of some components.
- Two different modes of viewing the menu entrys with multiple columns.
- New drag & drop options.
- Handle removable drives.

A more detail list can be found [here](https://gitlab.com/lestcape/Configurable-Menu/wikis/Some-features). And much more coming...

Help
--------------
Here you can find interesting information about Configurable Menu.
   - [Layouts](https://gitlab.com/lestcape/Configurable-Menu/wikis/Menu-layouts)
   - [Components](https://gitlab.com/lestcape/Configurable-Menu/wikis/Menu-components)
   - [Settings](https://gitlab.com/lestcape/Configurable-Menu/wikis/Settings)
   - [Cinnamon themes support](https://gitlab.com/lestcape/Configurable-Menu/wikis/Theme-Support)
   - [New languages](https://gitlab.com/lestcape/Configurable-Menu/wikis/Add-new-languages)

Change log
--------------
See a complete [Change log](https://gitlab.com/lestcape/Configurable-Menu/wikis/Change-Log).

0.9-Beta
   - Was added support for more languages.
   - Was added Spanish language.
   - Now you can have a different configuration for each layout.
   - The removable drives are now show in accessible panel and also can be removed.
   - The full screen mode was improved, now can be see better in more themes.
   - Now the property "Maximum width of the application entry text" it's really a maximum width and not the width.
   - Added support for the old Clutter.Color API to fix the bug "Clutter.Color.from_string is not a function" when you start the applet.
   - Fixed the bug on mint and windows 7 layouts. This error involves the closure of the menu in the case of the searchEntry do not has a focus.
   - The Menu now can be improved using the css file of any Cinnamon theme.
   - Removed the hover icon borders, due to allow more compatibility with cinnamon themes.
   - The new layouts of Vampire, Garibaldo, GnoMenu Left, GnoMenu Right GnoMenu Top and GnoMenu Bottom was added.
   - Several visually improvement in different layouts, especially the horizontal.
   - Now you can rename the apps on the accessible panel.
   - Changed the control buttons icons and was added the option to be symbolic or full color.
   - You can swap top and bottom menu panels.

This program is free software:
--------------
You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.

--------------
To report bugs, request new features and make suggestions, please visit:

https://gitlab.com/lestcape/Configurable-Menu/issues

You can also send us merge requests:

https://gitlab.com/lestcape/Configurable-Menu/merge_requests

--------------
Thank you very much for using this product.

Lester and Garibaldo.